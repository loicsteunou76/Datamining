import numpy as np
import h5py
import scipy.io
import matplotlib.pylab as plt
from matplotlib import style
style.use('ggplot')

colors = 10*["g","r","c","b"]

class K_Means:
    def __init__(self, nb_clusters=2, tol=0.001, max_iter=300):
        self.k = nb_clusters
        self.tol = tol
        self.max = max_iter

    def fit(self, X):
        np.random.shuffle(X)
        self.centroids = X[:self.k]

        for i in range(self.max):
            self.classification={}
            for j in range(self.k):
                self.classification[j]=[]

            for features in X:
                distances = np.sum((self.centroids-features)*(self.centroids-features), axis=1)
                self.classification[np.argmin(distances)].append(features)

            prev_centroids = self.centroids
            for key in self.classification.keys():
                self.centroids[key] = np.average(self.classification[key], axis=0)

    def predict(self, X_i):
        distances = np.sum((self.centroids-X_i)*(self.centroids-X_i), axis=1)
        classification = np.argmin(distances)
        return classification


X1 = np.random.rand(20,2)+0.5
X2 = np.random.rand(20,2)-0.5
X_train = np.concatenate((X1, X2), axis=0)
plt.scatter(X_train[:,0], X_train[:,1], color="k", s=60)
clf = K_Means(2)
clf.fit(X_train)
for i in range(clf.centroids.shape[0]):
    plt.scatter(clf.centroids[i][0], clf.centroids[i][1],
                marker="o", color=colors[i], s=150, linewidths=5)


X1 = np.random.rand(5,2)+0.5
X2 = np.random.rand(5,2)-0.5
X_test = np.concatenate((X1, X2), axis=0)


for X in X_test:
    classif = clf.predict(X)
    plt.scatter(X[0], X[1], marker="x", color=colors[classif])

plt.show()




#
