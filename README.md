# **Datamining**
Some of my Datamining project

# **Introduction folder**
In the Introduction folder you will find :
  * WhiteW.mat data : Info on this data given [here](http://archive.ics.uci.edu/ml/datasets/
Wine+Quality "white wine data").
  * A first easy data analyse.
  * A classification algorithm apply on this data : K-NN
  * Information on the K-NN algorithm (In French):
  ```
  La fonction de prédiction est basée sur la technique des k plus proches voisins. Elle consiste à considérer les points de (X_train , Y_train) comme les données de référence. Étant donnée un point quelconque X_i, elle détermine ses k plus proches voisins X_j dans X_a au sens d’une métrique (exemple : distance euclidienne). Le label qui sera affecté à X_i sera le label majoritaire de ses k voisins.
  ```
  * I used the [sklearn library](http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html#sklearn.neighbors.KNeighborsClassifier "K-NN with sklearn") in my script
  * The script name is K-NN.py


## **Clustering folder**
In the Clustering folder you will find :
  1. A course on clustering in Machine Learning
  2. A python script for an easy application of the K-Means algorithm (KMeans.py). You can run it using this line :
  ```
  python KMeans.py
  ```
  Should output a graph with :
    * In small black circle : the training set
    * In big circle : The Corresponding cluster
    * In small x : The results on the test set
    ![result](easykmeans.png?raw=true "result")
