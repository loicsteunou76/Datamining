import numpy as np
import h5py
import scipy.io
from sklearn import neighbors
import matplotlib.pylab as plt

print("----------EXERCISE 1----------\n")
mat = scipy.io.loadmat('WhiteW.mat')
X = mat["X"]
Y = mat["Y"]
print("Shape :")
print("Shape de X : ", X.shape)
print("Shape de Y : ", Y.shape, "\n")

print("MEAN :")
MOY = np.mean(X, axis=0)
for i in range(MOY.shape[0]):
    print("MEAN OF x{} = {}".format(i, MOY[i]))
print("\n")

print("VAR :")
VAR = np.var(X, axis=0)
for i in range(VAR.shape[0]):
    print("VAR of x{} = {}".format(i, VAR[i]))
print("\n")

print("STD :")
STD = np.std(X, axis=0)
for i in range(STD.shape[0]):
    print("STD of x{} = {}".format(i, STD[i]))
print("\n")

print("----------EXERCISE 2----------\n")
print("Data Processing :\n")
tr = 3000
val =  949
test = 949

permutation = np.random.permutation(X.shape[0])
X_shuffled = X[permutation]
Y_shuffled = Y[permutation]
Y_shuffled = np.squeeze(Y_shuffled)

X_train = X_shuffled[0:tr]
Y_train = Y_shuffled[0:tr]

X_val = X_shuffled[tr : tr+val]
Y_val = Y_shuffled[tr : tr+val]

X_test = X_shuffled[tr+val : tr+val+test]
Y_test = Y_shuffled[tr+val : tr+val+test]

dict_mean_acc_k = {}

print("K-nn algorithm indice of min listfor different values of k :\n")

k_val = []
acc_k = []

for k in range(5,1000,1):
    k_val.append(k)
    clf = neighbors.KNeighborsClassifier(k)
    clf.fit(X_train, Y_train)
    acc_k.append(clf.score(X_val,Y_val))

print("Plotting the error as a fct of k : \n")
plt.xlabel("k")
plt.ylabel("mean accuracy")
plt.plot(k_val,acc_k)


k_max = k_val[np.argmax(acc_k)]
print("Max accuracy reached for k = {}\n".format(k_max))
clf = neighbors.KNeighborsClassifier(k_max)
clf.fit(X_train, Y_train)
print("Score on test data (with k_max = {}) = {}\n".format(k_max, clf.score(X_test,Y_test)))

plt.show()















#
